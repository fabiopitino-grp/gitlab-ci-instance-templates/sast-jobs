# SAST jobs

Configure SAST jobs with GitLab CI/CD.

## How to use it

```yaml
include:
  - project: fabiopitino-grp/gitlab-ci-instance-templates/sast-jobs
    file: .gitlab-ci.yml
```

Then you can configure the following variables:

```yaml
variables:
  SAST_EXCLUDED_ANALYZERS: "flawfinder, kubesec" # to exclude specific SAST jobs
  SAST_DISABLED: true # to disable SAST jobs
```
